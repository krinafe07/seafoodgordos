import { Component, OnInit } from '@angular/core';
import { Auth } from 'src/app/Guards-Model-Pipes-Services/Model/auth.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/Guards-Model-Pipes-Services/Services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
title="Inicio de seción"
private emailPattern = "^[a-z0-9]+@[a-z0-9.-]+\.[a-z]{2,4}$";
user: Auth;
error;
CreateFormGroup() {
  return new FormGroup({
    email: new FormControl('', [Validators.required, Validators.pattern(this.emailPattern)]),
    password: new FormControl('', [Validators.required, Validators.minLength(8)])
  })
};

loginForm: FormGroup;
constructor(private auth: AuthService) { 
  this.loginForm = this.CreateFormGroup();
}

ngOnInit() {
}


login() {
  this.user = this.loginForm.value;
  this.auth.loginUser(this.user).subscribe(res => {
    console.log(res)
    localStorage.setItem('token', res.token)
    localStorage.setItem('rol', res.userExist.rol)
    localStorage.setItem('UserId', res.userExist.id)
    location.reload();
  }, err => {
    this.error = err.error;
  });
}

get email() { return this.loginForm.get('email') };
get password() { return this.loginForm.get('password') }

}
